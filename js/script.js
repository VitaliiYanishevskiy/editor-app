/*
* run this function, when page is loaded 
* to prevent manipulation with unexisted DOM elements
*/

window.onload = function () {
    
    /* Shourtcut functions to get DOM' nodes */
    function getE(a) {
        return document.getElementById(a);
    }

    function getT(a) {
        return document.getElementsByTagName(a);
    }

    function getS(a) {
        return document.querySelector(a);
    }

    /* Adding life to main page buttons for editing or styling content */
    getE("editBtn").addEventListener('click', function () {
        getE("stylier-area").style.display = "none";
        getE("editor-area").style.display = "block";
        getE("edit-textarea").value = getE("view").innerHTML;
    });

    getE("styleBtn").addEventListener('click', function () {
        getE("editor-area").style.display = "none";
        getE("stylier-area").style.display = "block";
    });

    /* Button to save result of editing by adding them to content area */
    getE("saveBtn").addEventListener('click', function () {
        getE("view").innerHTML = getE("edit-textarea").value;
    });
    
    /* Button to crear the result(content) area */
    getE("clearBtn").addEventListener('click', function(){
        getE("edit-textarea").value = "";
    });

    
    /* Working on styling area */
    
    /* Getting access to font-size and text-style forms */
    var fontSizeForm = document.forms.fontSize;
    var textStyleForm = document.forms.textStyle;

    /* Making font-size option using radio-buttons and the loop */
    for (var i = 0; i < fontSizeForm.length; ++i) {
        fontSizeForm[i].addEventListener('click', function () {
            getE("view").style.fontSize = this.value;
        });
    }

    /* Showing or hiding palettes for fons and background colors */
    document.body.addEventListener('click', function (e) {
        var target = e.target;
        if(target.id == "color-palette"){
            getE("bgcolors-pal").style.display = "none";
            getE("colors-pal").style.display = "flex";
        }
        else{
            getE("colors-pal").style.display = "none";
        }
        
        if(target.id == "bgcolor-palette"){
            
            getE("colors-pal").style.display = "none";
            getE("bgcolors-pal").style.display = "flex";
        }
        else{
            getE("bgcolors-pal").style.display = "none";
            
        }
    });

/*    getE("bgcolor-palette").addEventListener('click', function () {
    });*/

    
    var colors = document.querySelectorAll(".text-color");
    var bgColors = document.querySelectorAll(".bg-color");

    for (var i = 0; i < colors.length; ++i) {
        colors[i].addEventListener('click', function () {
            getE("view").style.color = this.style.backgroundColor;
        });
    }

    for (var i = 0; i < bgColors.length; ++i) {
        bgColors[i].addEventListener('click', function () {
            getE("view").style.backgroundColor = this.style.backgroundColor;
        });
    }

    textStyleForm.fontFamilyList.addEventListener('change', function () {
        for (var i = 0; textStyleForm.fontFamilyList.length; ++i) {
            if (textStyleForm.fontFamilyList[i].selected) {
                getE("view").style.fontFamily = textStyleForm.fontFamilyList[i].innerHTML + "";
            }
        }
    });

    textStyleForm.boldStyle.addEventListener('click', function () {
        if (this.checked) {
            getE("view").style.fontWeight = "bold";
        } else {
            getE("view").style.fontWeight = "normal";
        }
    });

    textStyleForm.italicStyle.addEventListener('click', function () {
        if (this.checked) {
            getE("view").style.fontStyle = "italic";
        } else {
            getE("view").style.fontStyle = "normal"
        }
    });

    getE("addBtn").addEventListener('click', function () {
        getS(".section-wrap").style.display = "none";
        getS(".btn-wrap").style.display = "none";
        getE("add").style.display = "block";
    });

    var addStructureForm = document.forms.addType;
    var addTableForm = document.forms.tableForm;
    var addListForm = document.forms.listForm;

    for (var i = 0; i < addStructureForm.length; ++i) {
        addStructureForm[i].addEventListener('click', function () {
            if (this.value == "table") {
                getE("listForm").style.display = "none";
                getE("tableForm").style.display = "block";
            } else if (this.value == "list") {
                getE("tableForm").style.display = "none";
                getE("listForm").style.display = "block";
            }
        });
    }

    addTableForm.createTable.addEventListener('click', function () {
        getE("add").style.display = "none";
        getS(".btn-wrap").style.display = "block";
        getS(".section-wrap").style.display = "flex";

        var numCols = addTableForm.numCols.value;
        var numRows = addTableForm.numCols.value;

        var sellWidth = addTableForm.sellWidth.value;
        var sellHeight = addTableForm.sellHeight.value;

        var borderThickness = addTableForm.borderThickness.value;
        var selectBorderType = addTableForm.borderLineType.value;
        var selectBorderColor = addTableForm.borderColor.value;

        var tableResult = "<table border=\"" + 1 + "\" style=\"border: " + borderThickness + "px " + selectBorderType + " " + selectBorderColor + "\">";

        for (var i = 0; i < numRows; ++i) {
            tableResult += "<tr>";
            for (var j = 0; j < numCols; ++j) {
                tableResult += "<td style=\"width:" + sellWidth + "px; height:" + sellHeight + "px;\">text</td>";
            }
            tableResult += "</tr>";
        }

        tableResult += "</table>"

        getE("edit-textarea").value += tableResult;

    });

    addListForm.createList.addEventListener('click', function () {
        getE("add").style.display = "none";
        getS(".btn-wrap").style.display = "block";
        getS(".section-wrap").style.display = "flex";

        var numListElem = addListForm.numListElem.value;
        var listMarking = addListForm.listMarking.value;

        var listResult = "<ul type=\"" + listMarking + "\">";

        for (var i = 0; i < numListElem; ++i) {
            listResult += "<li>Some Text</li>";
        }

        listResult += "</ul>";
        console.log(listResult);
        getE("edit-textarea").value += listResult;
    });

    addListForm.cancelBtn.addEventListener('click', function () {
        getE("add").style.display = "none";
        getS(".btn-wrap").style.display = "block";
        getS(".section-wrap").style.display = "flex";
    });
    
    addTableForm.cancelBtn.addEventListener('click', function () {
        getE("add").style.display = "none";
        getS(".btn-wrap").style.display = "block";
        getS(".section-wrap").style.display = "flex";
    });

    //    window.addEventListener('click', function(e){
    //        var target = e.target || e.srcElement;
    //        
    //        if(!(target.tagName == "section" && target.id == "colors-pal")){
    //            getE("colors-pal").style.display = "none";
    //        }
    //    });
    
    
/*    document.body.onclick = function(event){
        var target = event.target;
        if(target.id != "colors-pal"){
            document.querySelector(".ggg").style.display = ""
        }*/
        
        /*alert("click" + target);*/
/*    }*/
}